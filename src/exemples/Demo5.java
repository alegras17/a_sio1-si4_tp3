package exemples;

import java.util.Random;
import java.util.Scanner;

public class Demo5 {

    public static void main(String[] args) {
       
        Scanner  clavier=new Scanner(System.in);
        String   continuer="O"; 
        
        int nbADeviner;
        int nbPropose;
      
        int nbSucces=0,nbEssais=0;
        
        Random rd=new Random();
         
        while ( continuer.equals("O")){
        
          nbADeviner=rd.nextInt(10)+1;
          
          System.out.println("Proposez un nombre entre 1 et 10");
          nbPropose=clavier.nextInt();
        
          if(nbPropose == nbADeviner){
        
            System.out.println("Vous avez gagné!"); nbSucces++;
          }
          else{
        
            System.out.println("Vous avez perdu!");
            System.out.println("Le nombre à deviner était: "+ nbADeviner);
          }
      
          nbEssais++;
         
          
          System.out.println("\nVoulez vous rejouer? (répondre  O ou N)");
          continuer=clavier.next();
        }
        
        float tauxReussite=(float)nbSucces*100/nbEssais;
        System.out.printf("Vous avez gagné %2d fois sur %2d essais taux de réussite: %4.1f %%\n\n",nbSucces,nbEssais,tauxReussite);
        
    }
}

