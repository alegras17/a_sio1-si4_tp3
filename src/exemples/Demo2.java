package exemples;

import java.util.Scanner;

public class Demo2 {

    public static void main(String[] args) {
    
       Scanner  clavier   = new Scanner(System.in);
       
       int      depense=0;   
       int   sommeDispo=20;
        
       System.out.println("Vous disposez de 20 Euros\n");
       
       while( sommeDispo > 0){
             
           System.out.print("Saisir le somme dépensée? ");
           depense=clavier.nextInt();  
           
           if ( sommeDispo >= depense ){
               
               sommeDispo-=depense;  
               if( sommeDispo>0) System.out.printf("\nIl vous reste %2d Euros\n", sommeDispo);
           } 
           else{
               
               System.out.printf("\nVous ne pouvez faire cette dépense\n");
           }
        }
       
        System.out.println("\nVous avez dépensé les 20 Euros\n");
    }
}
