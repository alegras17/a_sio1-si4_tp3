package exemples;

import java.util.Random;
import java.util.Scanner;

public class Demo3 {

    public static void main(String[] args) {
    
       Scanner  clavier   = new Scanner(System.in);
       Random   aleat     = new Random();
        
       int de, total=0,  nbJets=0;
        
       while( total != 20 ){
             
           System.out.print("Tapez a et validez pour lancer le dé: ");
           clavier.next();  
         
           de=aleat.nextInt(6)+1;  
        
           if ( total+de <=20 ) total+=de; else total-=de;
           
           nbJets++;
          
           System.out.printf("Lancé N° %3d: vous avez sorti un: %2d Total: %2d\n",nbJets,de,total);
        }
       
       System.out.printf("\nVous avez atteint le total de 20 en %2d jets\n\n",nbJets);  
    }
}
