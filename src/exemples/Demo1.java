package exemples;

import java.util.Scanner;

public class Demo1 {

    public static void main(String[] args) {
        
        Scanner  clavier=new Scanner(System.in);
        String   continuer; 
        int      nbTours=0;
        
        System.out.print("Tapez oui pour commencer ou non pour abandonner ");
        continuer=clavier.next();
        
        while (continuer.equals("oui")){
        
            nbTours++;
            System.out.printf("Tour N° %2d\n",nbTours); 
            
            System.out.print("Tapez oui pour recommencer ou non pour arreter  ");
            continuer=clavier.next(); 
        }
        
        System.out.printf("\nvous avez effectué l'action %2d fois\n\n",nbTours);
    }
}
