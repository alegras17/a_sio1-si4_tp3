package exercices;

import java.util.Scanner;

public class Tp3Exo1 {

    public static void main(String[] args) {
   
    
        Scanner clavier = new Scanner(System.in);
        
        int note = 0;
        int nbNote = 0;
        float moyenne = 0;
        int noteMin = 1000;
        int noteMax = 0;
        int totalNote = 0;
        
         System.out.println("Entrez une note (-1 pour arrêter) :");
        note=clavier.nextInt();
        
        while (note != -1){
            
           
            nbNote++;
            totalNote = totalNote + note;
            
            if (note >= noteMax && note <= 20){
                
                noteMax = note;
                
            }
            
            if (note <= noteMin && note >= 0){
                
                    noteMin = note;
                
            }
            
            System.out.println("Entrez une note (-1 pour arrêter) :");
            note=clavier.nextInt();
        }
        
        moyenne = totalNote/nbNote;
        System.out.println("Nombre de note : " +nbNote);
        System.out.println("Moyenne : " +moyenne );
        System.out.println("Note Max : " +noteMax);
        System.out.println("Note Min : " +noteMin);
        
    }
}