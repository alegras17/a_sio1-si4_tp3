package exercices;

import java.util.Random;
import java.util.Scanner;


public class Tp3Exo3 {


    public static void main(String[] args) {
        
        Scanner clavier = new Scanner(System.in);
        int Essai = 0;
        int nbDeviner;
        int nb = 0;
        int stop = 0;
        
        
        System.out.println("Je viens de penser a un nombre entre 1 et 1000");
        System.out.println("Essayer de le deviner avec le moins d'essais possible \n");
        
        Random rd = new Random();
        nbDeviner = rd.nextInt(1000)+1;
        
        while ( nb != nbDeviner && stop == 0){
            
            System.out.println("Proposer un nombre ( 0 pour arreter ):");
            Essai++;
            nb = clavier.nextInt();
            
            if ( nb == 0){
                
                stop = 1;
                
            }
            
            else if( nb > nbDeviner){
                
                System.out.println("Trop grand");
                
            }
        
            else if ( nb < nbDeviner){
                
                System.out.println("Trop petit");
                
            }
            
            else{
                
                System.out.println("\nVous avez trouvez en " + Essai + " essais");
                
            }
            
        }
        
    }
}
